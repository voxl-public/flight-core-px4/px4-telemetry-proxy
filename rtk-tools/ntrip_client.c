/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "rtcm_packet.h"
#include "base64.h"
#include "tcp_utils.h"
#include "ntrip_client.h"

int64_t get_time_1us()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  int64_t tnow = (int64_t)(tv.tv_sec) * 1000000 + tv.tv_sec;
  return tnow;
}


int ntrip_receive_loop(ntrip_client_context_t * args)
{
  char read_buf[4096];
  int sleep_cycle_usec = 100000;

  rtcm3_packet_t packet;
  rtcm3_packet_init(&packet);

  int connected = 0;

  int64_t time_connect = 0;
  int64_t time_active  = 0;

  while(1)
  {
    if (connected == 0)
    {
      printf("ntrip_receive_loop: connecting to %s:%s@%s:%d/%s\n",args->username, args->password, args->hostname, args->port, args->mountpoint);
      args->fd = connect_to_ntrip_server(args->hostname, args->port, args->mountpoint, args->username, args->password);
      if (args->fd < 0)
      {
        printf("ntrip_receive_loop: error connecting.. sleeping for %d msec\n", args->retry_msec);
        usleep(args->retry_msec*1000);
        continue;
      }
      connected    = 1;
      time_connect = get_time_1us();
      time_active  = time_connect;
      rtcm3_packet_init(&packet);
    }

    //printf("start tcp read\n");
    int bytes_read = tcp_read(args->fd, read_buf, sizeof(read_buf));
    //printf("bytes read = %d\n",bytes_read);

    if (bytes_read > 0)
    {
      time_active = get_time_1us();
      for (int ii=0; ii<bytes_read; ii++)
      {
        int ret = rtcm3_packet_process_byte(&packet, read_buf[ii]);
        if (ret > 0)
        {
          uint16_t type = rtcm3_packet_get_payload_type(&packet);
          uint16_t size = rtcm3_packet_get_raw_size(&packet);
          uint8_t * raw = rtcm3_packet_get_raw_ptr(&packet);

          //printf("Got RTCM3 packet.. size = %d, type = %d\n", size,type);

          if (args->callback)
          {
            args->callback(raw,size,args->userdata);
          }
        }
        else if (ret < 0)
        {
          printf("ntrip_receive_loop: Error parsing the packet!\n");
        }
      }
    }

    int64_t time_now = get_time_1us();
    if (time_now - time_active > args->timeout_msec * 1000)
    {
      printf("ntrip_receive_loop: Connection stalled.. closing connection\n");
      tcp_disconnect(args->fd);
      args->fd = -1;
      connected = 0;
      //return -1;
    }

    usleep(sleep_cycle_usec);
  }

  return 0;
}



void *ntrip_receive_thread(void * args_in)
{
  printf("starting thread\n");
  ntrip_client_context_t * ctxt = (ntrip_client_context_t *)args_in;
  ntrip_receive_loop(ctxt);
  printf("exiting thread\n");
  pthread_exit(NULL);
}


int32_t connect_to_ntrip_server(const char *   hostname,
                                uint32_t       port,
                                const char *   mountpoint,
                                const char *   username,
                                const char *   password)
{
  const char * useragent  = "Test Client/0.0";

  //construct the request
  char buff[1024],user[512],*p=buff;
  memset(buff,0,sizeof(buff));

  //p+=sprintf(p,"GET %s/%s HTTP/1.0\r\n",hostname,mountpoint);  // does not work with some casters
  p+=sprintf(p,"GET /%s HTTP/1.1\r\n",mountpoint);
  p+=sprintf(p,"User-Agent: NTRIP %s\r\n",useragent);

  sprintf(user,"%s:%s",username,password);
  p+=sprintf(p,"Authorization: Basic ");
  p+=encbase64(p,(unsigned char *)user,strlen(user));
  p+=sprintf(p,"\r\n\r\n");  //need two sets of CR+LF, otherwise some casters don't work!

  //printf("Sending request:\n");
  //printf("%s",buff);

  int fd = tcp_connect(hostname,port);
  if (fd < 0)
  {
    printf("could not create fd\n");
    return -1;
  }

  int bytes_to_send = p-buff;
  int bytes_written = tcp_write(fd,buff,bytes_to_send);
  if ( bytes_written != bytes_to_send)
  {
    printf("send error\n");
		return -2;
	}

  //printf("wrote %d bytes\n",bytes_written);

  //TODO: check reply in case there was authentication failure, etc

  return fd;
}

void copy_string_to_buf_safe(const char * input, char * output, uint32_t output_max_size)
{
  uint32_t input_size = strlen(input) + 1;
  if (input_size > output_max_size)
  {
    printf("FATAL: copy_string_to_buf_safe: input size %d is greater than output max size %d\n", input_size, output_max_size);
    exit(1);
  }
  memcpy(output, input, input_size);
}

ntrip_client_id_t start_ntrip_client(
                            const char *   hostname,
                            uint32_t       port,
                            const char *   mountpoint,
                            const char *   username,
                            const char *   password,
                            void           (*rtcm3_callback)(uint8_t*,uint32_t,void*),
                            void       *   userdata
                          )
{

  //allocate memory for the actual context data
  ntrip_client_context_t * ctxt = calloc(1,sizeof(ntrip_client_context_t));
  if (ctxt == NULL)
  {
    printf("FATAL: failed to allocate memory for user context\n");
    return 0;
  }

  copy_string_to_buf_safe(hostname,   ctxt->hostname,   sizeof(ctxt->hostname));
  copy_string_to_buf_safe(mountpoint, ctxt->mountpoint, sizeof(ctxt->mountpoint));
  copy_string_to_buf_safe(username,   ctxt->username,   sizeof(ctxt->username));
  copy_string_to_buf_safe(password,   ctxt->password,   sizeof(ctxt->password));

  ctxt->port         = port;
  ctxt->fd           = -1;
  ctxt->timeout_msec = 5000;
  ctxt->retry_msec   = 5000;
  ctxt->callback     = rtcm3_callback;
  ctxt->userdata     = userdata;

  pthread_create(&ctxt->thread_id, NULL, ntrip_receive_thread, (void *)ctxt);

  return ctxt;
}

int32_t stop_ntrip_client(ntrip_client_id_t client_id)
{
  if ((int)client_id == -1)
    return -1;

  ntrip_client_context_t * ctxt = client_id;

  pthread_cancel(ctxt->thread_id);
  pthread_join(ctxt->thread_id, NULL);
  tcp_disconnect(ctxt->fd);
  free(ctxt);

  return 0;
}
