/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include "mav_packet_utils.h"

//MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN is 180 bytes

//pack a single mavlink gps rtcm message (complete or fragmented message (if > 180 bytes))
int32_t mav_pack_gps_rtcm_fragment(int fragmented, uint8_t fragment_id, uint8_t sequence_id,
                          uint8_t * payload, uint32_t payload_size, uint8_t * out_buf,
                          uint32_t out_buf_max_size, uint32_t * out_packet_size, int debug_flag)
{
  *out_packet_size = 0;

  if (debug_flag) printf("mav_pack_gps_rtcm_fragment: packing fragment %d, sequence %d, size %d\n", fragment_id, sequence_id, payload_size);

  //check if user has allocated enough buffer
  if (out_buf_max_size < 512) //256 is probably enough
  {
    fprintf(stderr, "mav_pack_gps_rtcm_fragment: output buffer is not large enough %d\n",out_buf_max_size);
    return -1;
  }

  //mavling packet generator expects the array to be MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN chars
  //even though the actual payload may be smaller
  uint8_t padded_payload[MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN];
  memset(padded_payload,0,sizeof(padded_payload));
  memcpy(padded_payload,payload,payload_size);

  //create and initialize mavlink message
  mavlink_message_t msg;
  memset(&msg,0,sizeof(msg));

  //TODO: set system_id and component_id to something other than zero?
  uint8_t system_id    = 0;
  uint8_t component_id = 0;
  uint8_t flags        = (sequence_id & 31) << 3 | (fragment_id & 3) << 1 | (fragmented & 1);
  uint16_t msg_size    = mavlink_msg_gps_rtcm_data_pack(system_id, component_id,
                                  &msg, flags, payload_size, padded_payload);  //returns size of mavlink structure size (not what we need)

  uint16_t out_size    = mavlink_msg_to_send_buffer(out_buf, &msg);  //convert from Mavlink msg structure to raw buffer

/*
  //print resulting packet
  printf("Mavlink Packet (%d bytes):\n",out_size);
  for (int ii=0; ii<out_size; ii++)
    printf("%d, ",out_buf[ii]);
  printf("\n");
*/

  *out_packet_size = out_size;

  return 0;
}
