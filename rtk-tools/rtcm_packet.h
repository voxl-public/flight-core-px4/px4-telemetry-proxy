#ifndef RTCM_PACKET_H
#define RTCM_PACKET_H

/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include "crc24q.h"

/*
rtcm3 message format:
+----------+--------+-----------+--------------------+----------+
| preamble | 000000 |  length   |    data message    |  parity  |
+----------+--------+-----------+--------------------+----------+
|<-- 8 --->|<- 6 -->|<-- 10 --->|<--- length x 8 --->|<-- 24 -->|
*/

#define RTCM3_PREAMBLE       0xD3
#define MAX_RTCM_PACKET_SIZE (1024+6)

typedef struct
{
  uint16_t num_parsed;
  uint16_t total_length;

  uint8_t buffer[MAX_RTCM_PACKET_SIZE];
} rtcm3_packet_t;

void rtcm3_packet_init(rtcm3_packet_t * packet)
{
  packet->num_parsed   = 0;
  packet->total_length = 0;
}

static inline uint32_t rtcm3_packet_get_raw_size(rtcm3_packet_t * packet)
{
  return packet->total_length;
}

static inline uint8_t * rtcm3_packet_get_raw_ptr(rtcm3_packet_t * packet)
{
  return packet->buffer;
}

static inline uint32_t rtcm3_packet_get_payload_size(rtcm3_packet_t * packet)
{
  if (packet->total_length > 6)
    return (uint32_t)(packet->total_length-6);
  return 0;
}

static inline uint8_t * rtcm3_packet_get_payload_ptr(rtcm3_packet_t * packet)
{
  return &(packet->buffer[3]);
}

static inline uint16_t rtcm3_packet_get_payload_type(rtcm3_packet_t * packet)
{
  uint16_t type  = ((uint16_t)packet->buffer[3]) << 4;
           type |= packet->buffer[4] >> 4;

  return type;
}

static inline int32_t rtcm3_packet_process_byte(rtcm3_packet_t * packet, uint8_t b)
{
  if (packet->num_parsed == 0)        // look for preamble
  {
    if (b != RTCM3_PREAMBLE)
      return 0;
    packet->buffer[packet->num_parsed++] = b;
    packet->total_length = 0;
    return 0;
  }

  if (packet->num_parsed == 1)        // second sync byte contains 6 MSB zeros and 2 bits of MSB of packet length
  {
    if ((b & 0b11111100) != 0)
    {
      packet->num_parsed = 0;
      return 0;
    }
    packet->buffer[packet->num_parsed++] = b;
    return 0;
  }

  if (packet->num_parsed == 2)        // third byte contains LSB byte of packet length
  {
    packet->buffer[packet->num_parsed++] = b;
    packet->total_length  = packet->buffer[2];
    packet->total_length |= ((uint16_t)(packet->buffer[1] & 0b00000011)) << 8;
    packet->total_length += 6;        // add 3 bytes for header and 3 for CRC for total packet length
    return 0;
  }

  if (packet->num_parsed == MAX_RTCM_PACKET_SIZE)  // this should never happen
  {
    packet->num_parsed   = 0;
    packet->total_length = 0;
    return 0;
  }

  packet->buffer[packet->num_parsed++] = b;

  if (packet->num_parsed < packet->total_length)
    return 0;

  // if we got here, we have received a complete packet
  packet->num_parsed = 0;   //reset the state

  // verify CRC24
  uint32_t crc_expected  = packet->buffer[packet->total_length-3]; crc_expected <<= 8;
           crc_expected |= packet->buffer[packet->total_length-2]; crc_expected <<= 8;
           crc_expected |= packet->buffer[packet->total_length-1];

  uint32_t crc_actual    = crc24q(packet->buffer,packet->total_length-3);

  //printf("CRC expected, actual = %X, %X\r\n",crc_expected, crc_actual);

  if ( crc_expected != crc_actual)
  {
    return -1;
  }

  return packet->total_length;
}


#endif //RTCM_PACKET_H
