#ifndef NTRIP_CLIENT_H
#define NTRIP_CLIENT_H

/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdint.h>
#include <pthread.h>

typedef struct
{
  char hostname[256];
  char mountpoint[64];
  char username[64];
  char password[64];
  uint32_t port;
  uint32_t timeout_msec;
  uint32_t retry_msec;
  void (*callback)(uint8_t*, uint32_t, void*);
  void * userdata;
  int fd;
  pthread_t thread_id;
} ntrip_client_context_t;

typedef ntrip_client_context_t* ntrip_client_id_t;

int ntrip_receive_loop(ntrip_client_context_t * args);

int32_t connect_to_ntrip_server(const char *   hostname,
                                uint32_t       port,
                                const char *   mountpoint,
                                const char *   username,
                                const char *   password
                               );

ntrip_client_id_t start_ntrip_client(
                            const char *   hostname,
                            uint32_t       port,
                            const char *   mountpoint,
                            const char *   username,
                            const char *   password,
                            void           (*rtcm3_callback)(uint8_t*,uint32_t,void*),
                            void       *   userdata
                          );

int32_t stop_ntrip_client(ntrip_client_id_t client_id);

#endif //NTRIP_CLIENT
