/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "tcp_utils.h"

#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int tcp_connect(const char * hostname, uint32_t port)
{
  int fd = -1;

  struct hostent * he;
  struct sockaddr_in server_addr;

  if ((he=gethostbyname(hostname)) == NULL)
  {
    printf("gethostbyname error\n");
    return -1;
  }

  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    printf("socket error\n");
    return -2;
  }

  server_addr.sin_family = AF_INET;
  server_addr.sin_port   = htons(port);
  server_addr.sin_addr   = *((struct in_addr *)he->h_addr);
  bzero(&(server_addr.sin_zero), 8);

  //set timeout
  struct timeval tv;
  tv.tv_sec  = 1;
  tv.tv_usec = 0;
  setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

  if (connect(fd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
  {
    printf("connect error\n");
    return -2;
  }

  return fd;
}

int tcp_disconnect(int fd)
{
  return close(fd);
}

int32_t tcp_write(int fd, char * buf, uint32_t size)
{
  return send(fd, buf, size, 0);
}

int32_t tcp_read(int fd, char * buf, uint32_t max_size)
{
  return recv(fd, buf, max_size, 0);
}
