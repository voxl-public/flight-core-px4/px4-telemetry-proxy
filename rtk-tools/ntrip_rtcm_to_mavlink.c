/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>

#include "ntrip_client.h"
#include "mav_packet_utils.h"

//NOTE: use mavlink v2 library : https://github.com/mavlink/c_library_v2

// compile with :
//gcc -O2 ntrip_rtcm_to_mavlink.c ntrip_client.c mav_packet_utils.c tcp_utils.c crc24q.c base64.c -o ntrip_rtcm_to_mavlink -lpthread -I<path_to_mavlink_v2/common>

//helper struct for passing sequence number back to the callback (called by internal thread)
typedef struct{
  uint32_t sequence_number;
} mav_rtcm_user_data_t;

//callback function that will be called by ntrip_client for every RTCM packet that comes in
void rtcm3_packet_callback(uint8_t * rtcm3_data, uint32_t rtcm3_data_size, void * userdata)
{
  printf("rtcm3_packet_callback: Got RTCM3 packet size %d\n",rtcm3_data_size);

  uint8_t out_buf[512]; //buffer to store the raw mavlink packet; max mavlink packet size is just over 256 bytes

  //get the pointer to the data passed in by the user
  mav_rtcm_user_data_t * client_data = (mav_rtcm_user_data_t*)userdata;

  //sanity checking
  uint32_t max_payload_size = MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN * 4; //up to 4 sub-frames allowed by mavlink
  if (rtcm3_data_size > max_payload_size)
  {
    printf("rtcm3_packet_callback: packet is too large : %d bytes\n",rtcm3_data_size);
    return;
  }

  //if payload size is greater than 180 bytes, it has to be broken up into chunks (not more than 4 chunks)
  int num_fragments = (int)(ceil(rtcm3_data_size/(float)(MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN)));
  //printf("num fragments %d\n",num_fragments);

  //sequence id will distinguish chunks from different payloads (in case there is some out-of-order arrival downstream)
  uint32_t sequence_id = (client_data->sequence_number++) % 32;  //only 5 bits for sequence id

  //pack the RTCM data into mavlink message(s)
  int frag;
  for (frag=0; frag<num_fragments; frag++)
  {
    uint32_t out_packet_size = 0;

    uint8_t * fragment_start = rtcm3_data + frag*MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN;
    uint32_t  fragment_size  = rtcm3_data_size <= MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN ?
                               rtcm3_data_size : (rtcm3_data_size-frag*MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN);
    if (fragment_size > MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN)
      fragment_size = MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN;

    int ret = mav_pack_gps_rtcm_fragment((int)(num_fragments > 1), frag, sequence_id,
                                fragment_start, fragment_size,
                                out_buf, sizeof(out_buf), &out_packet_size);

    //TODO: send data to serial port or something else
  }
  return;
}

int main(int argc, char *argv[])
{
  const char * hostname   = "132.239.154.80";
  uint32_t port           = 2103;
  const char * mountpoint = "P475_RTCM3";
  const char * username   = "YOUR_USERNAME";
  const char * password   = "YOUR_PASSWORD";

  mav_rtcm_user_data_t userdata;          //this is our structure for holding seq number, etc -- it will be passed to our callback function
  memset(&userdata,0,sizeof(userdata));

  ntrip_client_id_t client_id = start_ntrip_client(hostname, port, mountpoint, username, password, rtcm3_packet_callback, &userdata);

  for (int ii=0; ii<10; ii++)
  {
    usleep(1000000);
  }

  printf("Stopping the ntrip client\n");
  stop_ntrip_client(client_id);

  return 0;
}
