/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <voxl_io.h>
#include <mavlink/v2.0/common/mavlink.h>
#include <math.h>
#include "ntrip_client.h"
#include "mav_packet_utils.h"

#define REQUEST_PORT 50075
#define QGC_PORT 14550
#define MAX_DATA_BUFFER_LEN 1024
#define ADSB_UART_BAUD_RATE 57600
#define ADSB_UART_PORT UART_J12
#define PX4_UART_BAUD_RATE 57600
#define PX4_UART_PORT UART_J10

int debug = 0;

uint8_t* adsb_read_data_buffer;
uint8_t* write_data_buffer;

int px4_port = PX4_UART_PORT;

static int qgc_sockfd;
struct sockaddr_in qgc_servaddr;

// Telemetry data that needs to be pulled from the relevant mavlink messages
int satellites = 0;
double latitude = 0.0;
double longitude = 0.0;
double altitude_agl = 0.0;
double battery_volts = 0.0;
double battery_percentage = 0.0;
double heading = 0.0;
double ground_speed = 0.0;
double vertical_speed = 0.0;

// Helper struct for passing RTCM data sequence number back to the callback
typedef struct {
    uint32_t sequence_number;
} mav_rtcm_user_data_t;

void *adsb_thread(void *vargp) {
    mavlink_message_t msg;
    mavlink_status_t  msg_status;
    int last_message_sequence = 0;
    bool valid_message = false;
    uint8_t message_buffer[MAX_DATA_BUFFER_LEN];
    int bytes_read = 0;

    printf("ADSB thread starting\n");

    while (1) {
        usleep(1000);

        bytes_read = voxl_uart_read(ADSB_UART_PORT, adsb_read_data_buffer,
                                    MAX_DATA_BUFFER_LEN);

        for (int i = 0; i < bytes_read; i++)
        {
            if (mavlink_parse_char(0, adsb_read_data_buffer[i], &msg, &msg_status))
            {
                uint16_t msg_len = mavlink_msg_to_send_buffer(message_buffer, &msg);

                if (debug) {
                    printf("Received ADSB mavlink message with ID %d, sequence: %d. msg len %d\n",
                           msg.msgid, msg.seq, msg_len);
                    if (msg.seq != last_message_sequence + 1)
                    {
                        printf("********* ADSB MSG SEQUENCE FAIL ***********\n");
                    }

                    if (msg.seq == 255)
                    {
                        last_message_sequence = -1;
                    }
                    else
                    {
                        last_message_sequence = msg.seq;
                    }
                }

                if (msg.msgid == MAVLINK_MSG_ID_ADSB_VEHICLE)
                {
                    mavlink_adsb_vehicle_t *adsb_data = (mavlink_adsb_vehicle_t*) msg.payload64;

                    if ((adsb_data->lat != 0) && (adsb_data->lon != 0))
                    {
                        if (debug) printf("Sending MAVLINK_MSG_ID_ADSB_VEHICLE\n");

                        sendto(qgc_sockfd, message_buffer, msg_len,
                               MSG_CONFIRM, (const struct sockaddr*) &qgc_servaddr,
                               sizeof(qgc_servaddr));
                    }
                }
            }
        }
    }

    printf("ADSB thread ending\n");

    return NULL;
}

void *telemetry_thread(void *vargp) {
    printf("Telemetry thread starting\n");

    int sockfd;
    struct sockaddr_in servaddr, cliaddr;

    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("Request socket creation failed, exiting thread");
        return NULL;
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    // Filling server information
    servaddr.sin_family = AF_INET; // IPv4
    servaddr.sin_addr.s_addr = inet_addr("0.0.0.0");
    servaddr.sin_port = htons(REQUEST_PORT);

    // Bind the socket with the server address
    if (bind(sockfd, (const struct sockaddr *)&servaddr,
        sizeof(servaddr)) < 0 )
    {
        perror("request socket bind failed");
        return NULL;
    }

    printf("Telemetry thread waiting for requests\n");

    char request_buffer[MAX_DATA_BUFFER_LEN];
    char response_buffer[MAX_DATA_BUFFER_LEN];
    char build_buffer[MAX_DATA_BUFFER_LEN];

    socklen_t len = sizeof(cliaddr);
    int n;

    while (1)
    {
        n = recvfrom(sockfd, request_buffer, MAX_DATA_BUFFER_LEN,
                     MSG_WAITALL, (struct sockaddr *) &cliaddr,
                     &len);
        request_buffer[n] = '\0';
        if (debug) printf("Request : %s\n", request_buffer);

        bool valid_request = false;

        if (strstr(request_buffer, "type") != NULL) {
            if (strstr(request_buffer, "request") != NULL) {
                if (debug) printf("Got a valid request\n");

                valid_request = true;

                // TODO: Use the JSON C library for this
                sprintf(response_buffer, "{ \"latitude\": %.10f, ", latitude);
                sprintf(build_buffer, "\"longitude\": %.10f, ", longitude);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"altitude\": %.2f, ", altitude_agl);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"satellites\": %d, ", satellites);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"voltage\": %.2f, ", battery_volts);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"battery percentage\": %.2f, ",
                        battery_percentage);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"heading\": %.2f, ", heading);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"ground speed\": %.2f, ", ground_speed);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                sprintf(build_buffer, "\"vertical speed\": %.2f ",
                        vertical_speed);
                strncat(response_buffer, build_buffer, strlen(build_buffer));
                strncat(response_buffer, "}", 1);
            }
        }

        if (! valid_request)
        {
            fprintf(stderr, "Error: Invalid request\n");
            sprintf(response_buffer, "{ \"status\": \"invalid-request\" }");
        }

        sendto(sockfd, (const char*) response_buffer, strlen(response_buffer),
               MSG_CONFIRM, (const struct sockaddr*) &cliaddr,
               sizeof(cliaddr));
    }

    printf("Telemetry thread ending\n");
}

void *px4_transmit_thread(void *vargp) {
    printf("PX4 transmit thread starting\n");

    struct sockaddr_in qgc_recvaddr;
    uint32_t amount_read = 0;
    socklen_t len = sizeof(qgc_recvaddr);

    memset(&qgc_recvaddr, 0, sizeof(qgc_recvaddr));

    while (1)
    {
        // Receive UDP message from ground control
        amount_read = recvfrom(qgc_sockfd, (char*) write_data_buffer,
                               MAX_DATA_BUFFER_LEN, MSG_WAITALL,
                               (struct sockaddr*) &qgc_recvaddr, &len);

        if (debug) printf("Got %d bytes from ground control\n", amount_read);

        voxl_uart_write(px4_port, write_data_buffer, amount_read);

        usleep(100);
    }

    printf("PX4 transmit thread ending\n");

    return NULL;
}

// This is the callback function that will be called by ntrip_client for every
// RTCM packet that comes in.
void rtcm3_packet_callback(uint8_t* rtcm3_data, uint32_t rtcm3_data_size,
                           void* userdata) {
    if (debug) {
        printf("rtcm3_packet_callback: Got RTCM3 packet size %d\n",
               rtcm3_data_size);
    }

    // Buffer to store the raw mavlink packet
    // The max mavlink packet size is just over 256 bytes
    uint8_t out_buf[512];

    // Get the pointer to the data passed in by the user
    mav_rtcm_user_data_t* client_data = (mav_rtcm_user_data_t*) userdata;

    // Up to 4 sub-frames allowed by mavlink
    uint32_t max_payload_size = MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN * 4;
    if (rtcm3_data_size > max_payload_size)
    {
        fprintf(stderr, "rtcm3_packet_callback: packet is too large : %d bytes\n",
                rtcm3_data_size);
        return;
    }

    // If payload size is greater than 180 bytes, it has to be broken up into
    // chunks (not more than 4 chunks)
    int num_fragments = (int) (ceil(rtcm3_data_size /
                               (float) (MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN)));

    // The sequence id (5 bits only) will distinguish chunks from different
    // payloads (in case there is some out-of-order arrival downstream)
    uint32_t sequence_id = (client_data->sequence_number++) % 32;

    // Pack the RTCM data into mavlink message(s)
    int frag;
    for (frag = 0; frag < num_fragments; frag++)
    {
        uint32_t out_packet_size = 0;

        uint8_t* fragment_start = 0;
        fragment_start = rtcm3_data +
                         (frag * MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN);

        uint32_t fragment_size = rtcm3_data_size;
        if (rtcm3_data_size > MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN) {
            fragment_size = rtcm3_data_size -
                            (frag * MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN);
        }

        if (fragment_size > MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN) {
            fragment_size = MAVLINK_MSG_GPS_RTCM_DATA_FIELD_DATA_LEN;
        }

        int ret = mav_pack_gps_rtcm_fragment((int)(num_fragments > 1), frag,
                                             sequence_id, fragment_start,
                                             fragment_size, out_buf,
                                             sizeof(out_buf), &out_packet_size,
                                             debug);

        voxl_uart_write(PX4_UART_PORT, out_buf, out_packet_size);
    }

    return;
}


int main(int argc, char** argv) {
    int opt;
    int ret;
    int connect_to_ntrip = 0;
    int baudrate = PX4_UART_BAUD_RATE;
    int adsb_active = 0;

    char *qgc_address = "127.0.0.1";

    while((opt = getopt(argc, argv, "ai:b:d:gnh")) != -1) {
        switch(opt) {
        case 'i':
            qgc_address = optarg;
            break;
        case 'g':
            debug = 1;
            printf("Debug enabled\n");
            break;
        case 'a':
            adsb_active = 1;
            printf("ADS-B enabled\n");
            break;
        case 'n':
            connect_to_ntrip = 1;
            printf("NTRIP enabled\n");
            break;
        case 'h':
            printf("Parameters:\n");
            printf("\t-a\tSpecify ADS-B enabled, default = OFF\n");
            printf("\t-b\tSpecify baudrate, default = 57600\n");
            printf("\t-d\tSpecify port number, /dev/tty-X, default = 7\n");
            printf("\t-i\tSpecify ground control IP address\n");
            printf("\t-g\tEnable debug output, default = OFF\n");
            printf("\t-n\tConnect to San Diego NTRIP server, default = OFF\n");
            printf("\t-h\tView the supported parameters list\n");
            return 1;
        case 'b':
            baudrate = atoi(optarg);
            if(baudrate < 0) {
                printf("Error: negative baudrate provided\n");
                return -1;
            }
            break;
        case 'd':
            px4_port = atoi(optarg);
            if(px4_port < 0) {
                printf("Error: negative port number provided\n");
                return -1;
            }
            break;
        default:
            printf("unknown option: %c\n", optopt);
            break;
        }
    }

    printf("Using QGroundControl IP address %s\n", qgc_address);

    // Creating socket file descriptor for ground control connection
    if ((qgc_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        fprintf(stderr, "QGC socket creation failed");
        return -1;
    }

    memset(&qgc_servaddr, 0, sizeof(qgc_servaddr));

    // QGroundControl server information.
    qgc_servaddr.sin_family = AF_INET; // IPv4
    qgc_servaddr.sin_addr.s_addr = inet_addr(qgc_address);
    qgc_servaddr.sin_port = htons(QGC_PORT);

    uint8_t* read_data_buffer = voxl_rpc_shared_mem_alloc(MAX_DATA_BUFFER_LEN);
    if (read_data_buffer == NULL) {
        fprintf(stderr, "failed to allocate shared rpc memory\n");
        return -1;
    }

    write_data_buffer = voxl_rpc_shared_mem_alloc(MAX_DATA_BUFFER_LEN);
    if (write_data_buffer == NULL) {
        fprintf(stderr, "failed to allocate shared rpc memory\n");
        return -1;
    }

    if (adsb_active) {
        adsb_read_data_buffer = voxl_rpc_shared_mem_alloc(MAX_DATA_BUFFER_LEN);
        if (adsb_read_data_buffer == NULL) {
            fprintf(stderr, "failed to allocate shared rpc memory\n");
            return -1;
        }
    }

    int bytes_read = 0;

    mavlink_status_t mavlink_message_status;
    mavlink_message_t mavlink_message;

    printf("Initializing\n");

    ret = voxl_uart_init(px4_port, baudrate, 0, 1, 0);
    if (ret) {
        fprintf(stderr, "ERROR initializing PX4 UART\n");
        return -1;
    }

    if (adsb_active) {
        ret = voxl_uart_init(ADSB_UART_PORT, ADSB_UART_BAUD_RATE, 0, 1, 0);
        if (ret) {
            fprintf(stderr, "ERROR initializing ADS-B UART\n");
            return -1;
        }
    }

    // San Diego NTRIP server information
    const char* hostname = "132.239.154.80";
    uint32_t port = 2103;
    const char* mountpoint = "P472_RTCM3";
    // const char* mountpoint = "P475_RTCM3";
    const char* username = "YOUR_USERNAME";
    const char* password = "YOUR_PASSWORD";
    ntrip_client_id_t client_id = NULL;

    // This is our structure for holding RTCM seq number, etc
    mav_rtcm_user_data_t userdata;
    memset(&userdata,0,sizeof(userdata));

    // Start the NTRIP cllient to get RTCM data
    if (connect_to_ntrip) {
        client_id = start_ntrip_client(hostname, port, mountpoint, username,
                                       password, rtcm3_packet_callback,
                                       &userdata);
    }

    // Create thread to take received messages from ground control and transmit
    // to PX4 UART
    pthread_t px4_transmit_thread_id;
    pthread_create(&px4_transmit_thread_id, NULL, px4_transmit_thread, NULL);

    // Create thread to accept telemetry data requests and send back telemetry
    // data received from PX4
    pthread_t telemetry_thread_id;
    pthread_create(&telemetry_thread_id, NULL, telemetry_thread, NULL);

    pthread_t adsb_thread_id;
    if (adsb_active) {
        // Create thread to ADSB reports and send them off to QGC
        pthread_create(&adsb_thread_id, NULL, adsb_thread, NULL);
    }

    uint8_t message_buffer[MAX_DATA_BUFFER_LEN];
    uint16_t msg_len = 0;

    mavlink_global_position_int_t* position_data;
    mavlink_gps_raw_int_t* gps_data;
    mavlink_sys_status_t* status_data;
    mavlink_altitude_t* altitude_data;

    while (1) {
        usleep(10000);

        // Read data from UART
        bytes_read = voxl_uart_read(px4_port, read_data_buffer,
                                    MAX_DATA_BUFFER_LEN);

        if (bytes_read > 0) {
            if (debug) {
                printf("Read: %d Bytes\n", bytes_read);

                // Print read buffer content
                printf("\t\t[DEBUG] Received: ");
                for (int i = 0; i < bytes_read; i++) {
                    printf("%02X ", read_data_buffer[i]);
                }
                printf("\n");
            }

            for (int i = 0; i < bytes_read; i++) {
                if (mavlink_parse_char(0, read_data_buffer[i], &mavlink_message,
                                       &mavlink_message_status)) {
                    if (debug) {
                        printf("Got valid mavlink message. MSG ID %d\n",
                               mavlink_message.msgid);
                    }

                    msg_len = mavlink_msg_to_send_buffer(message_buffer,
                                                         &mavlink_message);

                    sendto(qgc_sockfd, message_buffer, msg_len,
                           MSG_CONFIRM, (const struct sockaddr*) &qgc_servaddr,
                           sizeof(qgc_servaddr));

                    switch (mavlink_message.msgid) {
                    case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
                        if (debug) printf("Got MAVLINK_MSG_ID_GLOBAL_POSITION_INT\n");
                        position_data = (mavlink_global_position_int_t*) mavlink_message.payload64;
                        // latitude = ((double) position_data->lat) / 10000000.0L;
                        // longitude = ((double) position_data->lon) / 10000000.0L;
                        heading = ((double) position_data->hdg) / 100.0;
                        vertical_speed = -((double) position_data->vz) / 100.0;
                        break;
                    case MAVLINK_MSG_ID_GPS_RAW_INT:
                        if (debug) printf("Got MAVLINK_MSG_ID_GPS_RAW_INT\n");
                        gps_data = (mavlink_gps_raw_int_t*) mavlink_message.payload64;
                        satellites = gps_data->satellites_visible;
                        ground_speed = ((double) gps_data->vel) / 100.0;
                        latitude = ((double) gps_data->lat) / 10000000.0L;
                        longitude = ((double) gps_data->lon) / 10000000.0L;
                        break;
                    case MAVLINK_MSG_ID_SYS_STATUS:
                        if (debug) printf("Got MAVLINK_MSG_ID_SYS_STATUS\n");
                        status_data = (mavlink_sys_status_t*) mavlink_message.payload64;
                        battery_volts = ((double) status_data->voltage_battery) / 1000.0;
                        battery_percentage = ((double) status_data->battery_remaining) / 100.0;
                        break;
                    case MAVLINK_MSG_ID_ALTITUDE:
                        if (debug) printf("Got MAVLINK_MSG_ID_ALTITUDE\n");
                        altitude_data = (mavlink_altitude_t*) mavlink_message.payload64;
                        altitude_agl = altitude_data->altitude_relative;
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }

    if (connect_to_ntrip) stop_ntrip_client(client_id);

    // Wait for the other threads to return
    pthread_join(telemetry_thread_id, NULL);
    pthread_join(px4_transmit_thread_id, NULL);
    if (adsb_active) {
        pthread_join(adsb_thread_id, NULL);
        voxl_rpc_shared_mem_free(adsb_read_data_buffer);
        ret = voxl_uart_close(ADSB_UART_PORT);
        if (ret) {
            fprintf(stderr, "ERROR closing ADS-B uart\n");
        }
    }

    printf("Shutting down\n");

    ret = voxl_uart_close(px4_port);
    if (ret) {
        fprintf(stderr, "ERROR closing PX4 uart\n");
    }

    // cleanup shared memory
    voxl_rpc_shared_mem_free(read_data_buffer);
    voxl_rpc_shared_mem_free(write_data_buffer);
    voxl_rpc_shared_mem_deinit();

    return 1;
}
